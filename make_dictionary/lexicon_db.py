#!/usr/bin/python3
# -*-coding:utf-8-*-
"""
@author: bibreen@gmail.com
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db_engine = create_engine('sqlite:///../resource/eunjeon_dic/eunjeon.sqlite')
session = None
table_name = 'lexicon_1_7'


def get_db_session():
    global session
    if session is None:
        session_class = sessionmaker(bind=db_engine)
        session = session_class()
    return session


def db_exist(surface, pos, semantic_class):
    global table_name
    ss = get_db_session()
    sql = """
        SELECT surface
        FROM %s
        WHERE surface=:surface AND pos=:pos
            AND semantic_class=:semantic_class AND is_available='1'
        """ % table_name
    row = ss.execute(
        sql, {'surface': surface,
              'pos': pos,
              'semantic_class': semantic_class}).fetchone()
    if not row:
        return False
    else:
        return True
