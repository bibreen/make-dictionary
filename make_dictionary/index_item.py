# -*-coding:utf-8-*-

__author__ = 'bibreen'


class IndexItem(object):
    def __init__(self,
                 surface,
                 pos,
                 semantic_class,
                 position_incr,
                 position_length):
        self.surface = surface
        self.pos = pos
        self.semantic_class = semantic_class
        self.position_incr = position_incr
        self.position_length = position_length

    def __repr__(self):
        return self.__dict__.__repr__()

    def __str__(self):
        return '/'.join([
            self.surface,
            self.pos,
            self.semantic_class,
        ])

    @staticmethod
    def create_index_items(node):
        surface = node.surface
        features = node.feature.split(',')
        pos = features[0]
        semantic_class = features[1]
        morpheme_type = features[4]
        index_expr = features[8]
        position_incr = '1'
        position_length = '1'
        output = []
        if morpheme_type != 'Compound' and morpheme_type != 'Preanalysis':
            output.append(IndexItem(surface, pos, semantic_class,
                                    position_incr, position_length))
        else:
            items = index_expr.split('+')
            for item in items:
                d = item.split('/')
                if d[3] != '0':
                    output.append(IndexItem(d[0], d[1], d[2], d[3], d[4]))
        return output
