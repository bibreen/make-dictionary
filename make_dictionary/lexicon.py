__author__ = 'kayden'


class Lexicon:
    def __init__(self, surface, pos, semantic_class, lexicon_type,
                 start_pos, end_pos, expr, index_expr, lexicon_class=None):
        self.surface = surface
        self.pos = pos
        self.semantic_class = semantic_class
        self.type = lexicon_type
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.expr = expr
        self.index_expr = index_expr
        self.lexicon_class = lexicon_class

    def __repr__(self):
        return self.__dict__.__repr__()

    def to_csv_string(self):
        # 가락국기,1784,3540,3081,NNP,*,F,가락국기,
        # Compound,*,*,가락국+기,가락/NNG/*/1/1+>가락국기/Compound/*/0/3+가락국/NNG/*/0/2+국/NNG/*/1/1+기/NNG/*/1/1
        l = [
            self.surface,
            '0', '0', '0',
            self.pos,
            self.semantic_class,
            '',
            self.surface,
            self.type,
            self.start_pos,
            self.end_pos,
            self.expr,
            self.index_expr
        ]
        if self.lexicon_class:
            l.append(self.lexicon_class)
        return ','.join(l)
