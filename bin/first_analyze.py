# -*-coding:utf-8-*-
import MeCab
from make_dictionary.index_item import IndexItem
from make_dictionary.lexicon import Lexicon
from make_dictionary.lexicon_db import *

__author__ = 'bibreen'


def main():
    # Wikipedia.csv에서 부작용이 일어나는 경우가 많아서 Wikipedia.csv 제거하고 실행하
    # 것이 좋음
    # 지명이 있을 경우 지명으로 선택하는 것이 구현 안됐다. 구현 필요
    tagger = MeCab.Tagger('-d /home/bibreen/eunjeon/mecab-ko-dic-1.6.1-no-wikipedia')

    with open('../data/raw_data/subway.txt') as f:
        bi_compound_file = open(
            '../data/dictionary/place-station/bi_compound_station.csv', 'w+')
        multi_compound_file = open(
            '../data/dictionary/place-station/multi_compound_station.csv', 'w+')
        while True:
            word = f.readline().strip()
            if not word:
                break
            head = word[0:-1]
            tail = word[-1:]

            index_exprs = []
            node = tagger.parseToNode(head)
            while node:
                if node.stat == MeCab.MECAB_EOS_NODE or \
                   node.stat == MeCab.MECAB_BOS_NODE:
                    node = node.next
                    continue
                # print(head)
                # print(node.surface, "\t", node.feature, node.stat)
                # print(node.surface)
                items = IndexItem.create_index_items(node)
                index_exprs.extend(items)
                node = node.next
            if len(items) == 1:
                first = items[0]
                if db_exist(first.surface, 'NNP', '지명'):
                    first.pos = 'NNP'
                    first.semantic_class = '지명'
            index_exprs.append(IndexItem(tail, 'NNG', '*', '1', '1'))
            # print(head, tail)
            expr_str = '+'.join(map(lambda x: x.surface, index_exprs))
            index_expr_str = '+'.join(map(lambda x: x.__str__(), index_exprs))
            lexicon = Lexicon(word, 'NNP', '지명', 'Compound',
                              #index_exprs[0].pos, index_exprs[-1].pos,
                              '*', '*',
                              expr_str, index_expr_str, 'Place-station')
            if len(index_exprs) <= 2:
                print(lexicon.to_csv_string())
                bi_compound_file.write(lexicon.to_csv_string() + '\n')
            else:
                print('multi', lexicon.to_csv_string())
                multi_compound_file.write(lexicon.to_csv_string() + '\n')
        bi_compound_file.close()
        multi_compound_file.close()


if __name__ == '__main__':
    main()
